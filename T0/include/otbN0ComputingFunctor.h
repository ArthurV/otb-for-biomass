/*=========================================================================

  Program:   ORFEO Toolbox
  Language:  C++
  Date:      $Date$
  Version:   $Revision$


  Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
  See OTBCopyright.txt for details.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef __otbDEMGeometryFunctor_h
#define __otbDEMGeometryFunctor_h
#include "otbDEMHandler.h"
#include <cmath>


namespace otb
{

/** \class Functor::MeanRatio
 *
 * - compute the ratio of the two pixel values
 * - compute the value of the ratio of means
 * - cast the \c double value resulting to the pixel type of the output image
 * - store the casted value into the output image.
 *

 * \ingroup Functor
 *
 * \ingroup OTBChangeDetection
 */
namespace Functor
{

template<class TInput, class TOutput> class DEMGeometryFunctor
{
public:
  otb::DEMHandler::Pointer DEMHandler;
  typedef otb::DEMHandler::PointType point;

  //double h1,h2,h3,h4;
  inline TOutput operator ()(const TInput& itA)
  {
	
	assert(nbPixels>0&&"Number of pixels in neighborhood is null");
    typename TInput::PixelType p1,p2,p3,p4;
    double h1,h2,h3,h4;
    TOutput outValue ;
    const double d2r = M_PI / 180;
    const double R=6378000.;
    double distance;

    outValue.SetSize(3);
    outValue.Fill(0.0);
    
    p1=itA.GetCenterPixel();
    outValue[0]=DEMHandler->GetHeightAboveEllipsoid( p1[0],p1[1]);

    p1=itA.GetNext(0);
    p2=itA.GetPrevious(0);
    p3=itA.GetNext(1);
    p4=itA.GetPrevious(1); 

    h1=DEMHandler->GetHeightAboveEllipsoid( p1[0],p1[1]);
    h2=DEMHandler->GetHeightAboveEllipsoid( p2[0],p2[1]);
    h3=DEMHandler->GetHeightAboveEllipsoid( p3[0],p3[1]);
    h4=DEMHandler->GetHeightAboveEllipsoid( p4[0],p4[1]);
    
    distance=R*acos(cos(p1[1]*d2r)*cos(p2[1]*d2r)*cos((p2[0]-p1[0])*d2r)+sin(p1[1]*d2r)*sin(p2[1]*d2r));
    outValue[1]=(h2-h1)/distance;     // Normal component along range
    distance=R*acos(cos(p3[1]*d2r)*cos(p4[1]*d2r)*cos((p4[0]-p3[0])*d2r)+sin(p3[1]*d2r)*sin(p4[1]*d2r));
    outValue[2]=(h4-h3)/distance;     // Normal component along azimuth
    
	return outValue;
  }
  void SetDEM (const otb::DEMHandler::Pointer dem)
  {
     DEMHandler = dem;
  }
};
}
} // end namespace otb

#endif
