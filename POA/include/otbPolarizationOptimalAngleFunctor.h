
#include "complex.h"
namespace otb
{

namespace Functor 
{

class PowellBoundedCostFunction : public itk::SingleValuedCostFunction 
{
	public:
	  typedef PowellBoundedCostFunction                  Self;
	  typedef itk::SingleValuedCostFunction   Superclass;
	  typedef itk::SmartPointer<Self>         Pointer;
	  typedef itk::SmartPointer<const Self>   ConstPointer;
	  itkNewMacro( Self );
	  itkTypeMacro( PowellBoundedCostFunction, SingleValuedCostFunction );
	
	  enum { SpaceDimension=1 };
  	
	  typedef Superclass::ParametersType      ParametersType;
	  typedef Superclass::DerivativeType      DerivativeType;
	  typedef Superclass::MeasureType         MeasureType ;
	  typedef std::complex<double> TParamType;
	  TParamType t12,t13,t22,t23,t33;
	  
	  PowellBoundedCostFunction()
	  {
	  }
  
	  void GetDerivative( const ParametersType & ,DerivativeType &  ) const
	  {
	  }

	  MeasureType GetValue( const ParametersType &parameters ) const
	  { 
	    double x = parameters[0];
	    MeasureType measure = pow(abs(-t12*sin(2.*x)+t13*cos(2.*x)),2)+pow(abs(-conj(t23)*pow(sin(2.*x),2)+t23*pow(cos(2.*x),2)+0.5*(t33-t22)*sin(4.*x)),2);
	    return measure;
	  }
	
	  unsigned int GetNumberOfParameters(void) const
	    {
    		return SpaceDimension;
	    }	


	private:


};

template <typename TInputPixel, typename TOutputPixel>  class VectorCoherencyMatrixToPOAFunctor
{
public:


inline TOutputPixel operator()(const TInputPixel& in) const
	{
	TOutputPixel outValue;
  	typedef  itk::PowellOptimizer  OptimizerType;
    
	  // Declaration of a itkOptimizer
	  OptimizerType::Pointer  itkOptimizer = OptimizerType::New();

	  // Declaration of the CostFunction 
	  PowellBoundedCostFunction::Pointer costFunction = PowellBoundedCostFunction::New();

	  itkOptimizer->SetCostFunction( costFunction.GetPointer() );
      costFunction->t12=in[1];
      costFunction->t13=in[2];
      costFunction->t22=in[3];
      costFunction->t23=in[4];
      costFunction->t33=in[5];
  
	  typedef PowellBoundedCostFunction::ParametersType    ParametersType;

	  const unsigned int spaceDimension =  costFunction->GetNumberOfParameters();

	  ParametersType  initialPosition( spaceDimension );

	  initialPosition[0] =  0.;
	  itkOptimizer->SetMaximize(false);
	  itkOptimizer->SetStepLength( 0.06 );
	  itkOptimizer->SetStepTolerance( 0.01 );
	  itkOptimizer->SetValueTolerance( 0.01 );
	  itkOptimizer->SetMaximumIteration( 1000);

  	  itkOptimizer->SetInitialPosition( initialPosition );

	  try 
	    {
	    itkOptimizer->StartOptimization();
	    }
	  catch( itk::ExceptionObject & e )
	    {
	    std::cout << "Exception thrown ! " << std::endl;
	    std::cout << "An error ocurred during Optimization" << std::endl;
	    std::cout << "Location    = " << e.GetLocation()    << std::endl;
	    std::cout << "Description = " << e.GetDescription() << std::endl;
	    return EXIT_FAILURE;
	    }

	  ParametersType finalPosition = itkOptimizer->GetCurrentPosition();
		outValue = static_cast<TOutputPixel>(finalPosition[0]);
		return outValue;
	}
};

}

}
