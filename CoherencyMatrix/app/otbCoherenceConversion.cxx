/*=========================================================================

 Program:   ORFEO Toolbox
 Language:  C++
 Date:      $Date$
 Version:   $Revision$


 Copyright (c) Centre National d'Etudes Spatiales. All rights reserved.
 See OTBCopyright.txt for details.


 This software is distributed WITHOUT ANY WARRANTY; without even
 the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the above copyright notices for more information.

 =========================================================================*/
#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbCoherenceMLImageFilter.h"
#include <iostream>
#include "otbDEMHandler.h"


namespace otb
{
namespace Wrapper
{

class CoherencyMatrix : public Application
{
public:
  /** Standard class typedefs. */
  typedef CoherencyMatrix   Self;
  typedef Application                         Superclass;
  typedef itk::SmartPointer<Self>             Pointer;
  typedef itk::SmartPointer<const Self>       ConstPointer;

  /** Standard macro */
  itkNewMacro(Self);

  itkTypeMacro(CoherencyMatrix, otb::Application);
  
  typedef otb::CoherenceMLImageFilter<ComplexDoubleVectorImageType,ComplexDoubleVectorImageType>CoherenceMLFilterType;
  
  
private:
  void DoInit()
  {
    SetName("CoherencyMatrix");
    SetDescription("");

    // Documentation
    SetDocName("CoherencyMatrix");
    SetDocLongDescription("" );
						  
    SetDocLimitations("None");
    SetDocAuthors("OTB-Team");
    SetDocSeeAlso("SARPolarMatrixConvert, SARPolarSynth");

    AddDocTag(Tags::SAR);
    
    AddParameter(ParameterType_ComplexInputImage,  "in",   "Input Image 1");
    SetParameterDescription("in", "Input polarimetric image");


    AddParameter(ParameterType_Int ,  "ml",   "Multilooking Windows Size");
    SetParameterDescription("ml", "Multilooking Windows Size");
    
    AddParameter(ParameterType_ComplexOutputImage, "out",  "Output coherence image");
    SetParameterDescription("out", "Output coherence image");
   
    AddRAMParameter();

    // Default values
  }

  void DoUpdateParameters()
  {
    // Nothing to do here : all parameters are independent
  }

  void DoExecute()
  {
    ComplexDoubleVectorImageType::Pointer inputPtr = GetParameterComplexDoubleVectorImage("in");
    int mlsize = GetParameterInt("ml");
    //std::cout << "inputPtr1: " << inputPtr1 << std::endl;
    //std::cout << "inputPtr2: " << inputPtr2 << std::endl;

    m_CoherenceFilter = CoherenceMLFilterType::New();
    m_CoherenceFilter->SetInput(inputPtr);
    m_CoherenceFilter->SetRadius(int((mlsize-1)/2));
    SetParameterComplexOutputImage("out", m_CoherenceFilter->GetOutput());


  }

  CoherenceMLFilterType::Pointer m_CoherenceFilter;
  
}; 

} //end namespace Wrapper
} //end namespace otb

OTB_APPLICATION_EXPORT(otb::Wrapper::CoherencyMatrix)

