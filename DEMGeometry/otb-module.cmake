set(DOCUMENTATION "Textures application (see also Textures module).")

otb_module(DEMGeometry
  DEPENDS
    OTBCommon
    OTBITK
    OTBImageBase
    OTBApplicationEngine

  TEST_DEPENDS

  DESCRIPTION
    "${DOCUMENTATION}"
)
